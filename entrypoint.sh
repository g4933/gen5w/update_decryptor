#!/bin/sh
# ... do some setup ...
# then run the CMD passed as command-line arguments
# exec "$@"

if grep -q EMPTY_DECRYPTION_KEY "/decryption_key.der"; then
  echo "Decryption Key has not been replaced with key from Headunit!!!"
  exit 1
fi


mkdir -p /mnt
cd /mnt
rm -rf ./decrypted
rm log 2>/dev/null ;
echo 0 > processed ;
find ./ -type f | wc -l > total_files_to_process;
find ./ -type f -exec bash -c '\
target="{}"; \
target_size="$(du -sb $target | awk NF=1)"; \
declare -i total_files_to_process=$(cat total_files_to_process) ;
declare -i files_processed=$(cat processed) ;
files_processed+=1 ;
echo $files_processed > processed;
mkdir -p ./decrypted/$(dirname $target); \
(/DecryptToPIPE -d /decryption_key.der $target 0 | pv -n -s $target_size | cat - > ./decrypted/$target) 2>&1 \
| dialog --begin 9 1 --title "Decryption log ($files_processed out of $total_files_to_process)" --tailboxbg log 20 120 --and-widget --begin 1 1 --title "Decrypting contents of $PWD" --gauge "Procesing [$target] " 6 70 0 ; \
echo "($(date)) - [$target] processed." >> log ' \; 

find ./decrypted -type f -empty -print -delete
find ./decrypted -type d -empty -print -delete
