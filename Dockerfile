ARG PLATFORM="linux/x86_64"
FROM --platform=${PLATFORM} alpine
RUN apk add gcompat pv dialog bash
COPY ./ /
RUN chmod +x /entrypoint.sh
RUN chmod +x /DecryptToPIPE
CMD ["/entrypoint.sh"]
